import 'package:flutter/material.dart';

var themeText = TextTheme(
  headline1: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.red),
  headline2: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.blue),
  headline3: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.green),
  headline4: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.yellow),
  headline5: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.pinkAccent),
  headline6: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.black38),//appBar
  subtitle1: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.lightGreenAccent),
  subtitle2: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.green),
  bodyText1: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.cyan),
  bodyText2: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.black),//main text body
  caption: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.yellowAccent),
  button: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.indigo),
  overline: TextStyle(fontSize: 26, fontWeight: FontWeight.bold, color: Colors.deepOrangeAccent),
);

var themeApp = ThemeData(
  primaryColor: Colors.black,
  textTheme: themeText,
  primaryTextTheme: themeText,
//  accentTextTheme: themeText,
);
