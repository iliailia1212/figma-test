import 'package:bank_test/store/models/AppStore.dart';
import 'package:bank_test/widgets/appBar/appBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

void main() {
  debugPaintSizeEnabled = false;
  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AppStore()),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
//      theme: themeApp,
      home: Scaffold(
        appBar: appBar,
        body: Container(
          width: double.infinity,
          color: Colors.green,
          child: SingleChildScrollView(
            child: Consumer<AppStore>(
            builder: (context, store, child){
              return FutureBuilder(
                future: store.fetchList(),
                builder: (context, snapshot){
                  print(snapshot);
                  var list = store.listData.data;
                  var count = list != null ? list.length : 0;
                  print('count main');
                  print(count);
                  print(store.listData);
                  return Column(
                    children: [
                      for(int i = 0; i < count; i++) Text(list[i]['id'].toString())
                    ],
                  );
                },
              );
            })
          ),
        ),
      ),
    );
  }
}
