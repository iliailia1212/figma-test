class Transaction {

  double cost;
  final DateTime date;
  String label;

  Transaction({
    this.cost,
    this.date,
    this.label
  });
}