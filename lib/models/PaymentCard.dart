import 'package:flutter/material.dart';

import 'Transaction.dart';

class PaymentCard {
  String cardNumber;
  double balance;
  String name;
  List<Transaction> transactionList = [];

  PaymentCard({
    @required this.cardNumber,
    this.balance,
    this.name,
    this.transactionList,
  });

  String toString2() {
    return 'PaymentCard{cardNumber: $cardNumber, balance: $balance, name: $name, transactionList: $transactionList}';
  }
}
