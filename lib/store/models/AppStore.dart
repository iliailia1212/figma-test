import 'dart:convert';

import 'package:bank_test/models/PaymentCard.dart';
import 'package:bank_test/models/Transaction.dart';
import 'package:bank_test/store/fetch/StoreFetch.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

var list = [
  for (var i = 0; i < 10; i++)
    PaymentCard(
        cardNumber: '000000000$i',
        name: 'Card number: $i',
        balance: 1000.0 * i,
        transactionList: [
          for (var i = 0; i < 4; i++)
            Transaction(date: DateTime.now(), cost: 200, label: 'Label: $i')
        ])
];

class AppStore extends StoreFetch {
  int _cardSliderActiveIndex = 0;
  List<Object> _albums;

  List<PaymentCard> paymentCardList = list;

  AppStore() : super('cars');

  int get cardSliderActiveIndex => _cardSliderActiveIndex;

  set cardSliderActiveIndex(int value) {
    _cardSliderActiveIndex = value;
    notifyListeners();
  }

  set albums(albums) {
    _albums = albums;
    notifyListeners();
  }
}
