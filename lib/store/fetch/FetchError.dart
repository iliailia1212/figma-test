class FetchError {
  final int statusCode;
  final String message;
  final Object fields;

  FetchError({code, data})
      : statusCode = data,
        message = data?.message,
        fields = data?.fields;
}
