import 'dart:convert';

import './FetchStatus.dart';
import './FetchError.dart';
import 'package:http/http.dart' as http;

class FetchData {
  FetchStatus status = FetchStatus.none;
  FetchError error;
  var data;

  bool get isFetched {
    return this.status != FetchStatus.none;
  }

  void setPending() {
    this.setData(null);
    status = FetchStatus.pending;
  }

  void set(http.Response response){
    var data = jsonDecode(response.body);
    if(response.statusCode == 200) {
      status = FetchStatus.done;
      this.setData(null);
      error = null;
    }
    else {
      status = FetchStatus.error;
      error = FetchError(code: response.statusCode, data: data);
    }
    this.setData(data);
  }

  void setData(Map<String, dynamic> data) {
    this.data = data;
  }

  void setError(http.Response response){

  }
}
