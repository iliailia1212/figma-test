enum FetchStatus {
  none,
  pending,
  done,
  error,
}