class PaginationMeta {
  int current;
  int last;
  int totalItems;

  PaginationMeta({this.current, this.last, this.totalItems});

  PaginationMeta.fromJson(Map<String, dynamic> json) {
    this.current = json['current_page'];
    this.last = json['last_page'];
    this.totalItems = json['total'];
  }
}