import 'package:bank_test/store/fetch/FetchData.dart';
import 'package:bank_test/store/fetch/PaginationMeta.dart';

class FetchDataPagination extends FetchData {
  PaginationMeta meta;

  void setData(Map<String, dynamic> data) {

    if(data != null) {
      print('meta');
      print(data['meta']);
    }
    if(data != null && data['meta'] != null) {
      if (this.meta == null || data['meta']['current_page'] >= this.meta.current) {
        print('length');
        print(data['data'].length);
        this.data = data['data'];
      }
      else {
        print('[...]');
        this.data = [...?this.data, ...?data['data']];
      }
      this.meta = PaginationMeta.fromJson(data['meta']);
    }
    else {
      this.data = data;
    }
  }
}
