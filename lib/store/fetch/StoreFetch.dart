import 'package:bank_test/store/fetch/FetchDataPagination.dart';
import 'package:flutter/cupertino.dart';

import './FetchData.dart';
import 'package:http/http.dart' as http;

const BASE_URL = 'https://api.rentride.ru/api/2.0/';

class StoreFetch with ChangeNotifier {
  final listData = FetchDataPagination();
  final singleData = FetchData();
  final updateData = FetchData();
  final addData = FetchData();
  final deleteData = FetchData();

  final String path;

  StoreFetch(this.path);

  _fetch() {
    return http.get(BASE_URL + this.path);
  }

  fetchList() {
    if(!this.listData.isFetched) {
      listData.setPending();
      return this._fetch().then((http.Response response) {
        listData.set(response);
        notifyListeners();
        return response;
      }, onError: (e) {
        print(e); //TODO
      });
    }
  }

  fetchByAlias() {}

  fetchUpdate() {}

  fetchAdd() {}

  fetchDelete() {}
}
