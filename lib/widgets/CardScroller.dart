import 'package:bank_test/store/models/AppStore.dart';
import 'package:flutter/animation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CardScroller extends StatelessWidget {
  final controller = PageController(
    viewportFraction: 0.8,
  );

  @override
  Widget build(BuildContext context) {

    return Consumer<AppStore>(
      builder: (context, store, child){
        var list = store.paymentCardList;
        if (controller.hasClients) {
          controller.animateToPage(1,
              duration: Duration(seconds: 1), curve: Curves.bounceIn);
        }

        return Container(
          height: 200,
          child: PageView.builder(
            itemCount: list.length,
            controller: controller,
            itemBuilder: (context, index) {
              return Container(
                margin: EdgeInsets.all(10),
                color: Colors.lightBlueAccent,
                child: Text("slide#${store.cardSliderActiveIndex} ;${list[index].toString2()}"),
              );
            },
          ),
        );
      },
    );
  }
}
