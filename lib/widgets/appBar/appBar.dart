import 'package:flutter/material.dart';

import '../ContainerApp.dart';
import 'Avatar.dart';

var appBar = PreferredSize(
  preferredSize: Size.fromHeight(150),
  child: SafeArea(
    child: Center(
      child: SizedBox(
          height: 50,
          child: ContainerApp(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text('My Cards'),
                Avatar()
              ],
            ),
          )
      ),
    ),
  ),
);