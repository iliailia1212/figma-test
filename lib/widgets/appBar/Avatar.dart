import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image.network(
        'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg',
        frameBuilder: (BuildContext context, Widget child, int frame, bool wasSynchronouslyLoaded) {
          return Container(
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: Colors.yellow,
              borderRadius: BorderRadius.all(Radius.circular(50))
            ),
            child: ClipOval(child: child),
          );
        }
    );
  }
}
