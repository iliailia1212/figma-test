import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ContainerApp extends StatelessWidget {
  ContainerApp({@required this.child, this.padding});

  final Widget child;
  final EdgeInsetsGeometry padding;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? EdgeInsets.symmetric(horizontal: 16),
      child: child,
    );
  }
}
